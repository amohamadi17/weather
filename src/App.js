import React, { useState } from "react";

const App = () => {
  const [inputValue, setValeu] = useState("");
  const [weather, setWeather] = useState({});

  const API = {
    keyApi: "090ea4cc8bb8fb907cabd69dbcde9c0d",
    Base: "https://api.openweathermap.org/data/2.5/",
  };

  const getApiHandler = async (e) => {
    if (e.key === "Enter") {
      fetch(
        `${API.Base}weather?q=${inputValue}&units=metric&APPID=${API.keyApi}`
      )
        .then((response) => response.json())
        .then((data) => {
                setWeather(data)
                setValeu('')
        });

      console.log(weather);
    }
  };
  const getDateHandler = () => {
    const days = [
      "Sunday",
      "Monday",
      "Tuesday",
      "Wednesday",
      "Thursday",
      "Friday",
      "Saturday",
    ];

    const months = [
      "January",
      "February",
      "March",
      "April",
      "May",
      "June",
      "July",
      "August",
      "September",
      "October",
      "November",
      "December",
    ];

    const d = new Date();
    let day = days[d.getDay()];
    let month = months[d.getMonth()];
    let year = d.getFullYear();

    let date = "";
    date = d.toString();
    let nowDate = date.slice(8, 10);

    return `${day} ${nowDate} ${month} ${year} `;
  };
  return (
    <main className="app">
      <div className="search-box">
        <input
          placeholder="City weather search"
          type="search"
          onChange={(e) => setValeu(e.target.value)}
          onKeyPress={(e) => getApiHandler(e)}
          value={inputValue}
        />
      </div>

      {typeof weather.main != "undefined" ? (
        <div className="result-box">
          <h2>
            {weather.name} , {weather.sys.country}
          </h2>
          <p className="date">{getDateHandler()}</p>
          <p className="result">{Math.floor(weather.main.temp)}{`\xB0`} C</p>
        </div>
      ) : (
        ""
      )}
    </main>
  );
};

export default App;
